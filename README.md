# One million books

Fork of [One million books](https://github.com/hugoruivo/bookslist)

Inside the mybooks.zip you will find a JSON file with more than a million books entries. The data of the file where generated using: [Random Book JSON Generator](https://github.com/hugoruivo/randombookjsongenerator).

## Book JSON Format

```json
{
	"name":"Book Title",
	"author":
	{
		"name":"Author Name",
		"gender":"male or female"
	},
	"genre":"Book genre",
	"publish_date":"Date in format: yyyy-mm-dd"
}
```
